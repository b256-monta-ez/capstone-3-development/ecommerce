import { Form, Button } from 'react-bootstrap';
import { useContext, useState, useEffect } from 'react';
// import userContext from '../userContext';

export default function Login() {
	// user validation
	// const { user, setUser } = useContext(UserContext);

	// hooks for input fields
	const { email, setEmail } = useState('');
	const { password, setPassword } = useState('');

	// if button is enabled or disabled
	const { isActive, setIsActive } = useState(false);

	useEffect(() => {

		if(email !== '' && password !== '') {

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	})


	return(

	    <Form className="mt-5">
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Enter password" />
	      </Form.Group>
	      {
	      	isActive ?
	      		<Button variant="primary" type="submit"> Submit </Button>
	      	:
	      		<Button variant="primary" type="submit" disabled> Submit </Button>
	      }
	    </Form>
	)
}