import { Table } from 'react-bootstrap';

export default function Admin() {

	return (
		<title className="text-center">Admin Dashboard</title>

	    <Table striped bordered hover size="sm">
	      <thead>
	        <tr>
	          <th>Name</th>
	          <th colSpan={2}>Description</th>
	          <th>Price</th>
	          <th>Availability</th>
	          <th>Actions</th>
	        </tr>
	      </thead>
	      <tbody>
	        <tr>
	          <td>1</td>
	          <td>Mark</td>
	          <td>Otto</td>
	          <td>@mdo</td>
	        </tr>
	        <tr>
	          <td>2</td>
	          <td>Jacob</td>
	          <td>Thornton</td>
	          <td>@fat</td>
	        </tr>
	        <tr>
	          <td>3</td>
	          <td>Larry the Bird</td>
	          <td>@twitter</td>
	        </tr>
	      </tbody>
	    </Table>
	)
}