import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function AppNavBar () {

	return(

	<Navbar bg="dark" expand="lg">
      <Container>
        <Navbar.Brand href="#home" className="text-light">Shoepee</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home" className="text-light">Home</Nav.Link>
            <Nav.Link href="#products" className="text-light">Products</Nav.Link>
            <Nav.Link href="#products" className="text-light">Login</Nav.Link>
            <Nav.Link href="#products" className="text-light">Register</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)

}