import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import AppNavBar from './components/AppNavBar';
import Login from './pages/Login';
import Register from './pages/Register';
import Admin from './pages/Admin';


function App() {
  return (
    <Router>
      <AppNavBar />
      <Container>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path='/admin' element={<Admin />} />
        </Routes>
      </Container>
    </Router>
  )
}

export default App;
